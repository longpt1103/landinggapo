
$(document).ready(function(){
    new WOW().init();
    $('#slider-class').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        autoplay:true,
        autoplaySpeed: 0,
        speed: 4000,
        easing:'linear',
        cssEase:'linear',
        pauseOnHover:false,
        pauseOnFocus: false,
        swipe:false,
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
    
      ]
    });
    var deadline = new Date("septem 25, 2019 23:59:59").getTime();             
    var x = setInterval(function() {
    var currentTime = new Date().getTime();                
    var t = deadline - currentTime; 
    var days = Math.floor(t / (1000 * 60 * 60 * 24)); 
    var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
    var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60)); 
    var seconds = Math.floor((t % (1000 * 60)) / 1000); 
    document.getElementById("day").innerHTML =days ; 
    document.getElementById("hour").innerHTML =hours; 
    document.getElementById("minute").innerHTML = minutes; 
    document.getElementById("second").innerHTML =seconds; 
    if (t < 0) {
        clearInterval(x); 
        document.getElementById("time-up").innerHTML = "TIME UP"; 
        document.getElementById("day").innerHTML ='0'; 
        document.getElementById("hour").innerHTML ='0'; 
        document.getElementById("minute").innerHTML ='0' ; 
        document.getElementById("second").innerHTML = '0'; 
    } 
    }, 1000); 
  });